FILES		= rapport.md

LANG		= french
TITLE		= "Semaine entreprise - GISTRE 2019"
SUBTITLE	= ""
AUTHOR		= "Julien DOCHE"
DATE		= "2018"
PROMO		= 2019

DIR_TEMPLATE	= .
TEMPLATE	= template
DOC		= semaine_entreprise
TYPE		= latex

include build.mk
